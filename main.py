"""
Rock paper suka
"""

import random

from utils import format_string
from core import IRockPaperScissors


class RockPaperScissors(IRockPaperScissors):
    # Change it in instance if you need it
    SPELLS = {
        # can_hit  - может побить
        # cant_hit - не может
        0: {
            "name": "Камень",
            "can_hit": [1],
            "cant_hit": [2]
        },

        1: {
            "name": "Ножницы",
            "can_hit": [2],
            "cant_hit": [0],
        },

        2: {
            "name": "Бумага",
            "can_hit": [0],
            "cant_hit": [1]
        },
    }

    DEFAULT_MAX_SCORE = 3

    def __init__(self, max_score=None):
        super().__init__(max_score)
        self._user_score = 0
        self._npc_score = 0
        self._max_score = max_score
        # нихуя себе
        self._running = False

        if max_score is None:
            self._max_score = self.DEFAULT_MAX_SCORE

    def _printer(self, message, role=None):
        print(format_string(message, role))

    def _player_input(self):
        self._show_spells()
        value = input(format_string("Ввод: ", role="Player", end=" "))
        value = int(value)
        spell = self._get_spell(value - 1)
        self._printer("Вы выбрали - {!r} | {}".format(spell["name"], value), role="Player")

        return value

    def _npc_input(self):
        self._printer("Хожу . . .", role="Robot")
        value = random.randint(1, self._max_score)
        spell = self._get_spell(int(value) - 1)

        self._printer("Я выбрал - {!r}! | {}".format(spell["name"], value), role="Robot")

        return value

    def _get_spell(self, value):
        try:
            return self.SPELLS[value]

        except KeyError:
            print(
                format_string(f"Неправильный ключ! ({value})\n")
            )

        except ValueError:
            print(
                format_string(f"Неверный тип данных! ({type(value)})\n")
            )

    def _show_spells(self):
        items = ", ".join(
            "{!s} - {!s}".format(key + 1, val["name"]) for (key, val) in self.SPELLS.items()
        )

        self._printer(items)


rps = RockPaperScissors()
rps.main_loop()
