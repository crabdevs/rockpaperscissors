"""
RPS interface
"""

from abc import ABC, abstractmethod

from utils import format_string


class IRockPaperScissors(ABC):
    SPELLS = NotImplemented
    DEFAULT_MAX_SCORE = None

    def __init__(self, max_score=None):
        self._user_score = 0
        self._npc_score = 0
        self._max_score = max_score
        self._running = False

        if self._max_score is None:
            self._max_score = self.DEFAULT_MAX_SCORE

    @abstractmethod
    def _printer(self, message):
        """
        Show formatted message
        """

    @abstractmethod
    def _player_input(self):
        """
        Player input event
        :return: SPELLS index
        """

    @abstractmethod
    def _npc_input(self):
        """
        Generates random number from 1 to N
        :return: SPELLS index
        """

    @abstractmethod
    def _show_spells(self):
        """
        Method for showing SPELLS
        :return:
        """

    def _get_spell(self, value):
        try:
            return self.SPELLS[value]

        except KeyError:
            self._printer(
                format_string(f"Неправильный ключ! ({value})\n")
            )

        except ValueError:
            self._printer(
                format_string(f"Неверный тип данных! ({type(value)})\n")
            )

        self._show_spells()

    def _battle_handler(self, player_v, npc_v):
        """
        Battle handler - handles battle
        :param player_v: player value
        :param npc_v: npc value
        :return: message
        """
        if player_v == npc_v:
            self._printer("wow!")

        if npc_v - 1 in self._get_spell(player_v - 1)["can_hit"]:
            self._user_score += 1
            self._printer("Вы получили одно очко")

        if npc_v - 1 in self._get_spell(player_v - 1)["cant_hit"]:
            self._npc_score += 1
            self._printer("Робот получил одно очко")

        self._printer(
            f"Вы: {self._user_score} | Робот: {self._npc_score}"
        )

        if (
            self._user_score == self._max_score
            and self._user_score > self._npc_score
        ):
            self._printer("Вы победили!")
            self._running = False

        if (
            self._npc_score == self._max_score
            and self._npc_score > self._user_score
        ):
            self._printer("Вы проиграли!")
            self._running = False

    def main_loop(self):
        self._running = True

        while self._running:
            player_choice = self._player_input()
            npc_choice = self._npc_input()
            self._battle_handler(player_choice, npc_choice)
