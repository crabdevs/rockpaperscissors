from .irps import IRockPaperScissors

__all__ = (
    "IRockPaperScissors",
)
