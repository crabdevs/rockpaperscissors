"""
String utils
"""


def format_string(message, role=None, prefix=None, sep=None, end=None):
    if not role:
        role = "System"

    if not prefix:
        prefix = ""
    prefix = " {} ".format(prefix)

    if not sep:
        sep = " "

    if not end:
        end = ""

    separator = "{sep}".format(sep=sep)
    if isinstance(message, (tuple, list)):
        message = separator.join(message)

    output_str = "[{role:<12}]{prefix:>3}{message}{end}".format(
        role=role,
        prefix=prefix,
        message=message,
        end=end
    )
    return output_str
